# JEAF Generator Meta Model README #

This repository contains the JEAF Generator Meta Model. The meta model is required in MagicDraw UML models when working with JEAF and JEAF Generator. 


## Links ##
For further information please refer to our documentation:

* [How to setup MagicDraw UML Projects](https://anaptecs.atlassian.net/l/c/JExsVrNc)
* [JEAF Generator](https://anaptecs.atlassian.net/l/c/N7r5x11X)
* [JEAF Modelling Guide](https://anaptecs.atlassian.net/l/c/1B2ci31g)

## How do I get set up? ##

* [How to setup MagicDraw UML Projects](https://anaptecs.atlassian.net/l/c/JExsVrNc)
